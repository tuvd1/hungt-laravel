<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'user' => 'required|unique:users,user',
            'email' => 'required|email:rfc|unique:users,email',
            'gender' => 'required',
            'address' => 'required',
            'action' => 'required',
            'role_id' => 'required|exists:roles,id',
            'department_id' => 'required|exists:departments,id',
            'position_id' => 'required|exists:positions,id',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên Nhân viên không được bỏ trống',
            'user.required' => 'User không được bỏ trống',
            'email.required' => 'Email không được bỏ trống',
            'gender.required' => 'Bạn chưa chọn giới tính',
            'address.required' => 'Địa chỉ của nhân viên không được bỏ trống',
            'action.required' => 'Tình trạng nhân viên chưa được chọn',
            'department_id.required' => 'Phòng ban không được bỏ trống',
            'position_id.required' => 'Chức vụ Nhân viên không được bỏ trống',
            'password.required' => 'Bạn chưa nhập password',
            'confirm_password.required' => 'Bạn chưa nhập confirm_password',
            'user.unique' => 'Tên User đã tồn tại',
            'email.email' => 'Định dạng email không hợp lệ',
            'email.unique' => 'Email đã tồn tại',
            'confirm_password.same' => 'Passowrd nhập lại không đúng',
        ];
    }
}
