<?php

namespace App\Models;
use Illuminate\Support\Facades\Hash;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'user',
        'email',
        'gender',
        'first_login',
        'role_id',
        'department_id',
        'position_id',
        'address',
        'action',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function department(){
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function role(){
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function position(){
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }

    public function changePasswords(){
        return $this->hasMany(ChangePassword::class, 'user_id', 'id');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function scopeSearchUser($query)
    {
        if ($search = request()->search) {
            $query = $query->where('name', 'like', '%' . $search . '%');
        }
        return $query;
    }

    public function scopeActionUser($query)
    {
        if (isset(request()->action) && request()->action != null) {
            $query = $query->where('action', request()->action);
        }
        return $query;
    }

    public function sendMail($blade,$token,$check){ 
        Mail::send(
            $blade,
            compact(['check','token']),
            function ($message) use ($check) {
                $message->to($check->email, $check->name);
                $message->subject('Reset mật khẩu');
            }
        );
    }

}
