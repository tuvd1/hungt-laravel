<?php
  
namespace App\Exports;
  
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
  
class UsersExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::with(['position', 'department' ])->select(['id','name','user','email','gender'])->get();
    }
    public function map($users): array
    {
        return [
            $users->id,
            $users->name,
            $users->user,
            $users->email,
            $users->gender == 0? "Nam" : "Nữ" ,
           
        ];
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function headings(): array
    {
        return ["ID", "Name", "User", "Email", 'Giới tính'];
    }
}
