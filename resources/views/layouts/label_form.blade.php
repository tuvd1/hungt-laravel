     <div class="form-group">
         <label for="">{{ $label ?? 'Label' }}</label>
         <input type="{{ $type ?? 'text' }}" {{ $atribute ?? "" }}   class="{{ $class ?? 'form-control' }}" attt value="{{ $value ?? '' }}"
             name="{{ $name ?? '' }}" placeholder="{{ $placeholder ?? '' }}">
         @error($name)
             <span class=" text-danger">{{ $message }}</span>
         @enderror
     </div>
