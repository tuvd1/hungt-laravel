<div class="form-group">
    <label for="{{ $name ?? 'name' }}">{{ $label ?? 'name' }}</label>
    <select class="form-control" required name="{{ $name ?? 'name' }}" id="{{ $name ?? 'name' }}">
        <option value="">---Chọn {{ $label }}---</option>
        @foreach ($data as $item)
            <option value="{{ $item->id }}" {{ isset($id) && $id == $item->id ? 'selected' : '' }}>
                {{ $item->name }}</option>
        @endforeach
    </select>
    @error($name)
        <span class=" text-danger">{{ $message }}</span>
    @enderror
</div>
