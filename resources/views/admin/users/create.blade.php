@extends('layouts.main')
@section('css')
    <style>
        .wapper {
            margin: 0 50px;
        }
    </style>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('user.index') }}">List User</a></li>
    <li class="breadcrumb-item active">Create User</li>
@endsection

@section('content')
    <div class="wapper">
        @include('layouts.alert')
        <form action="{{ route('user.store') }}" method="post">
            @csrf
            @include('layouts.label_form', [
                'label' => 'Fulle Name',
                'name' => 'name',
                'placeholder' => 'Mời bạn nhập Tên ...',
                'value' => old('name'),
            ])

            @include('layouts.label_form', [
                'label' => 'User',
                'name' => 'user',
                'placeholder' => 'Mời bạn nhập User ...',
                'value' => old('user'),
            ])

            @include('layouts.label_form', [
                'label' => 'Email',
                'name' => 'email',
                'placeholder' => 'Mời bạn nhập Email ...',
                'value' => old('email'),
            ])

            <div class="form-group">
                <label for="">Giới tính</label> &elinters;
                <input type="radio" name="gender" checked value="0">Nam
                <input type="radio" name="gender" value="1">Nữ
            </div>

            @include('layouts.select_form', [
                'label' => 'Department',
                'name' => 'department_id',
                'data' => $data['departments'],
            ])

            @include('layouts.select_form', [
                'label' => 'Position',
                'name' => 'position_id',
                'data' => $data['positions'],
            ])

            @include('layouts.select_form', [
                'label' => 'Role',
                'name' => 'role_id',
                'data' => $data['roles'],
            ])

            @include('layouts.label_form', [
                'label' => 'Address',
                'name' => 'address',
                'placeholder' => 'Mời bạn nhập địa chỉ ...',
                'value' => old('address'),
            ])

            <div class="form-group">
                <label for="">Tình trạng</label> &elinters;
                <input type="radio" name="action" checked value="0">Dang làm
                <input type="radio" name="action" value="1">Nghỉ làm
            </div>

            @include('layouts.label_form', [
                'label' => 'Password',
                'name' => 'password',
                'type' => 'password',
                'placeholder' => 'Mời bạn nhập Password ...',
            ])

            @include('layouts.label_form', [
                'label' => 'Re-password',
                'name' => 'confirm_password',
                'type' => 'password',
                'placeholder' => 'Mời bạn nhập lại Password ...',
            ])
            <button type="submit" name="addUser" class="btn btn-success">ADD User</button>
        </form>
    </div>
    </div>
@endsection
