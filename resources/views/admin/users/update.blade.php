@extends('layouts.main')
@section('css')
    <style>
        .wapper {
            margin: 0 50px;
        }
    </style>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('user.index') }}">User</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('content')
    <div class="wapper">
        @include('layouts.alert')
        <form action="{{ route('user.update', $user->id) }}" method="post">
            @csrf
            @method('put')
            <input type="hidden" value="{{ $user->id }} " name="id">
            @include('layouts.label_form', [
                'label' => 'Fulle Name',
                'name' => 'name',
                'placeholder' => 'Mời bạn nhập Tên ...',
                'value' => $user->name,
            ])

            @include('layouts.label_form', [
                'label' => 'User',
                'name' => 'user',
                'placeholder' => 'Mời bạn nhập User ...',
                'value' => $user->user,
                'atribute' => 'readonly',
            ])

            @include('layouts.label_form', [
                'label' => 'Email',
                'name' => 'email',
                'placeholder' => 'Mời bạn nhập Email ...',
                'value' => $user->email,
            ])

            <div class="form-group">
                <label for="">Giới tính</label> &elinters;
                <input type="radio" name="gender" {{ Auth::user()->gender == 0 ? 'checked' : '' }} value="0">Nam
                <input type="radio" name="gender" {{ Auth::user()->gender == 1 ? 'checked' : '' }} value="1">Nữ
            </div>

        
            @include('layouts.select_form', [
                'label' => 'Department',
                'name' => 'department_id',
                'data' => $data['departments'],
                'id' => $user->department->id
            ])

            @include('layouts.select_form', [
                'label' => 'Pepartment',
                'name' => 'position_id',
                'data' => $data['positions'],
                'id' => $user->position->id
            ])

            @include('layouts.select_form', [
                'label' => 'Role',
                'name' => 'role_id',
                'data' => $data['roles'],
                'id' => $user->role->id
            ])     

            @include('layouts.label_form', [
                'label' => 'Address',
                'name' => 'address',
                'placeholder' => 'Mời bạn nhập địa chỉ ...',
                'value' => $user->address,
            ])

            <div class="form-group">
                <label for="">Tình trạng</label> &elinters;
                <input type="radio" name="action" {{ Auth::user()->action == 0 ? 'checked' : '' }} value="0">Đang
                làm
                <input type="radio" name="action" {{ Auth::user()->action == 1 ? 'checked' : '' }} value="1">Nghỉ
                làm
            </div>

            @include('layouts.label_form', [
                'label' => 'Password',
                'name' => 'password',
                'type' => 'password',
                'value' => '',
                'placeholder' => 'Mời bạn nhập Password ...',
            ])

            @include('layouts.label_form', [
                'label' => 'Re-password',
                'name' => 'confirm_password',
                'type' => 'password',
                'value' => '',
                'placeholder' => 'Mời bạn nhập lại Password ...',
            ])
            <button type="submit" name="addUser" class="btn btn-success">Edit User</button>
        </form>
    </div>
    </div>
@endsection
