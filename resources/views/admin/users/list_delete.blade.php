@extends('layouts.main')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('user.index') }}">Danh sách User</a></li>
    <li class="breadcrumb-item active">Danh sách User đã xóa</li>
@endsection

@section('content')
    @include('layouts.alert')
    <table class="table table-light">
        <thead class="thead-light">
            <tr>
                <th width= "10%">STT</th>
                <th>Full Name</th>
                <th>User</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $key => $user)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->user }}</td>
                    <td>
                        <div class="row">
                            <a href="{{ route('user.restore', $user->id) }}"  onclick="return confirm('Are you sure ?')" class="btn btn-warning"><i class="fa fa-undo"></i></a> &ensp;
                            <form action="{{ route('user.delete.over', $user->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure ?')">
                                    <i class="fa fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach

        </tbody>

    </table>
@endsection
