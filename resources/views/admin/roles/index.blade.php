@extends('layouts.main')
@section('css')
@endsection

@section('js')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item active">Role</li>
@endsection

@section('content')
    <div>
        @include('layouts.alert')
        <table class="table table-light table-hover">
            <thead class="thead-light">
                <tr>
                    <th width=5%>STT</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $role)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            <div class="row">
                                <a href="{{ route('role.edit', $role->id) }}" class="btn btn-warning"><i
                                        class="fa fa-edit"></i></a> &ensp;
                                <form action="{{ route('role.destroy', $role->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                                        <i class="fa fa-trash"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
@endsection
