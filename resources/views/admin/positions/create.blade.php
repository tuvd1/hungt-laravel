@extends('layouts.main')
@section('css')
@endsection

@section('js')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('position.index') }}">Position</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('content')
    @include('layouts.alert')
    <form action="{{ route('position.store') }}" method="post">
        @csrf
        @include('layouts.label_form', [
            'label' => 'Position Name',
            'name' => 'name',
            'placeholder' => 'Mời bạn nhập Tên Position ...',
            'value' => old('name'),
        ])
         @include('layouts.label_form', [
            'label' => 'Pority',
            'name' => 'pority',
            'placeholder' => 'Số càng nhỏ ưu tiên càng cao ',
            'value' => old('name'),
        ])
        <button type="submit" name="addPosition" class="btn btn-primary">Thêm Position</button>
    </form>
@endsection
