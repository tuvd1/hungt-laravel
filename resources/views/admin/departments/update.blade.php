@extends('layouts.main')
@section('css')
@endsection

@section('js')
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
    <li class="breadcrumb-item "><a href="{{ route('department.index') }}">Department</a></li>
    <li class="breadcrumb-item active">Update</li>
@endsection

@section('content')
    <form action="{{ route('department.update', $role->id) }}" method="post">
        @csrf
        @method('put')
        @include('layouts.label_form', [
            'label' => 'Departments Name',
            'name' => 'name',
            'placeholder' => 'Mời bạn nhập Tên Department ...',
            'value' => $department->name,
        ])
        <button type="submit" name="addDepartment" class="btn btn-primary">Sửa Department</button>
    </form>
@endsection

