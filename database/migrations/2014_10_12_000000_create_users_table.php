<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name',191);
            $table->string('user',191)->unique();
            $table->string('email')->unique();
            $table->tinyInteger('gender')->default(0)->comment('1 => nữ, 0 => nam');
            $table->string('address',191);
            $table->tinyInteger('action')->default(0)->comment('0=>đang làm , 1=>đã nghỉ');
            $table->tinyInteger('first_login')->default(0)->comment('0=>chưa đổi , 1=>đã đổi');
            $table->tinyInteger('role_id');
            $table->tinyInteger('department_id');
            $table->tinyInteger('position_id');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
